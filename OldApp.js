/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import React from 'react';
import {
  SafeAreaView,
  StatusBar,
  StyleSheet,
  Text,
  useColorScheme,
  View,
} from 'react-native';

import {
  Colors,
} from 'react-native/Libraries/NewAppScreen';
import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from "@react-navigation/native-stack"
import HomeScreen from "./screens/HomeScreen";
import AboutScreen from "./screens/AboutScreen";

const Stack = createNativeStackNavigator();

function App() {
  const isDarkMode = useColorScheme() === 'dark';
  
  const backgroundStyle = {
    backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
  };

  return (
    // <SafeAreaView style={backgroundStyle}>
      <NavigationContainer>
        <Stack.Navigator initialRouteName='Home'>
          <Stack.Screen name="Home" component={HomeScreen}/>
          <Stack.Screen name='About' component={AboutScreen}/>
        </Stack.Navigator>
      </NavigationContainer>
    // </SafeAreaView>
  );
}


export default App;



//====================================================================================================================


// import { NavigationContainer,useNavigation } from "@react-navigation/native";
// import { createNativeStackNavigator } from "@react-navigation/native-stack";
// import {View, Text, Button} from "react-native"

// function HomeScreen() {
//   const navigation = useNavigation();
//   return (
//     <View>
//       <Text>Home Screen</Text>
//       <Button title="Go to About" onPress={()=>navigation.navigate("About")}/>
//     </View>
//   );
// }

// function AboutScreen() {
//   return (
//     <View>
//       <Text>About Screen</Text>
//     </View>
//   );
// }

// const Stack = createNativeStackNavigator();
// export default function App() {
//   return (
//     <NavigationContainer>
//      <Stack.Navigator initialRouteName="Home">
//         {/*Define our routes*/}
//         <Stack.Screen name="Home" component={HomeScreen} />
//         <Stack.Screen name="About" component={AboutScreen} />
//       </Stack.Navigator>
//     </NavigationContainer>
//   );
// }
