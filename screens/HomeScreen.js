import {
  View,
  Text,
  Button,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {useNavigation} from '@react-navigation/native';
import {useState} from 'react';
// import OpenAI from 'openai';
import { OpenAI, isReactElement, useChat } from 'react-native-gen-ui';
// const configuration = new Configuration({
//   apiKey: process.env.OPEN_API_KEY,
// });
// const openai = new OpenAI({
//     apiKey: "sk-proj-wParIZKkbp3B0iACMh8dT3BlbkFJAnsZ5urejs1ZucQLS5b3" // This is the default and can be omitted
//   });

const openAi = new OpenAI({
    apiKey: "sk-proj-wParIZKkbp3B0iACMh8dT3BlbkFJAnsZ5urejs1ZucQLS5b3",
    model: 'gpt-4',
    // You can even set a custom basePath of your SSE server
  });
const HomeScreen = () => {
  const navigation = useNavigation();
  const [input, setInput] = useState('');
  const [output, setOutput] = useState('');

  const handleInput = async () => {
    try {
    //   const userInput = constPrompt + input;
    //   const response = await openai.completions.create({
    //     model: 'text=devinci-003',
    //     prompt: `You: ${input}\nAI`,
    //     temperature: 0,
    //     max_token: 60,
    //     top_p: 1.0,
    //     frequency_penalty: 0.5,
    //     presence_penalty: 0.0,
    //     stop: ['You:'],
        
    //   });
    //   const response = await openai.chat.completions.create({
    //     messages: [{ role: 'user', content: input }],
    //     model: 'gpt-4',
    //     stream: true,
    //     temperature: 0,
    //     max_tokens: 60,
    //     top_p: 1.0,
    //     frequency_penalty: 0.5,
    //     presence_penalty: 0.0,
    //     stop: ['You:'],
    //   });
    //   for await (const chunk of response) {
    //     // process.stdout.write();
    //     setOutput(chunk.choices[0]?.delta?.content || '');
    //   }
    // const { input, onInputChange, messages, isLoading, handleSubmit } =
     useChat({
        openAi,
        // Optional initial messages
        initialMessages: [
          { content: input, role: 'system' },
        ],
        // Optional success handler
        onSuccess: (messages) => {
            setOutput(messages)
            console.log('Chat success:', messages)
        },
        // Optional error handler
        onError: (error) => console.error('Chat error:', error),
      });
      
    } catch (error) {
      console.log(error);
    }
    setInput('');
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>AI Chatbot GPT 4</Text>
      {/* <Button title="Go to About" onPress={()=>navigation.navigate("About")}/> */}
      <View style={styles.chatContainer}>
        <View style={styles.inputContainer}>
          <TextInput
            style={styles.input}
            placeholder="Type your message here"
            onChangeText={text => setInput(text)}
            value={input}>
          </TextInput>
          <TouchableOpacity style={styles.sendButton} onPress={handleInput}>
              <Text style={styles.sendButtonText}>Send</Text>
            </TouchableOpacity>
            <View style={styles.outputContainer}>
              <Text style={styles.output}>{output}</Text>
            </View>
        </View>
      </View>
    </View>
  );
};
export default HomeScreen;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    marginBottom: 20,
    fontSize: 24,
    fontWeight: 'bold',
  },
  chatContainer: {
    width: '90%',
    height: '70%',
    borderWidth: 1,
    borderRadius: 10,
    overflow: 'hidden',
  },
  inputContainer: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 10,
    backgroundColor: '#F2F2F2',
  },
  input: {
    flex: 1,
    height: 40,
    borderWidth: 1,
    borderRadius: 20,
    padding: 10,
    marginRight: 10,
    backgroundColor: '#fff',
  },
  sendButton: {
    backgroundColor: '#2196f3',
    padding: 10,
    borderRadius: 20,
  },
  sendButtonText: {
    color: '#fff',
    fontWeight: 'bold',
    textAlign: 'center',
  },
  outputContainer: {
    flex: 1,
    padding: 10,
    backgroundColor: '#fff',
  },
  output: {
    fontSize: 16,
  },
});
