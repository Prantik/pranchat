import {
  View,
  Text,
  StyleSheet,
  TextInput,
  FlatList,
  ActivityIndicator,
  Button,
} from 'react-native';
import React, {useState, useEffect} from 'react';
import * as GoogleGEnerativeAI from '@google/generative-ai';
import Tts from 'react-native-tts';

const AboutScreen = () => {
  const API_KEY = 'AIzaSyB6C443oXwGtL7o7ZC7yNqTOHNQYZ9JF10';

  // Chat History
  let [message, setMessage] = useState([]);
  // User Typed Input
  const [userInput, setUserInput] = useState('');
  // Loading while feating response on user input
  const [loading, setLoading] = useState(false);

  const sendMessage = async () => {
    setLoading(true);
    const userMessage = {
      question: userInput,
      ans: '',
      user: true,
      isLoading: true,
    };
    setMessage([...message, userMessage]);
    const genAI = new GoogleGEnerativeAI.GoogleGenerativeAI(API_KEY);
    const model = genAI.getGenerativeModel({model: 'gemini-pro'});

    const prompt = userMessage.question;

    const result = await model.generateContent(prompt);
    const response = result.response;
    const text = response.text();
    setMessage([
      ...message,
      {
        question: userInput,
        ans: text,
        user: false,
        isLoading: false,
      },
    ]);
    
    if (text !== '') {
        try {
            Tts.speak(text);
        } catch (error) {
            console.log(error);
        }
        
    }
    setUserInput('');
    setLoading(false);
  };

  cleanChatHistory = () => {
    setMessage([]);
  };

  const renderItem = ({item}) => (
    <View key={item.text} style={style.chatContainer}>
      <View style={[style.bubbleMainContainer, {alignItems: 'flex-start'}]}>
        <View style={[style.bubble, {marginRight: 2}]}>
          <Text style={style.bubbleText}>P</Text>
        </View>
        <View
          style={[
            style.userInputContainer,
            {borderBottomLeftRadius: 30, paddingStart: 10, marginRight: 10},
          ]}>
          <Text style={[style.userInputTextDesign]}>{item.question}</Text>
        </View>
      </View>
      <View
        style={[
          style.bubbleMainContainer,
          {alignItems: 'flex-end', alignSelf: 'flex-end'},
        ]}>
        <View
          style={[
            style.userInputContainer,
            {
              alignItems: 'flex-end',
              marginLeft: 20,
              paddingEnd: 10,
              paddingBottom:20,
              borderBottomRightRadius: 80,
            },
          ]}>
          {item.isLoading ? (
            <ActivityIndicator
              style={{padding: 5}}
              size={'small'}
              color="red"
            />
          ) : (
            <Text style={style.messageHistory}>{item.ans}</Text>
          )}
        </View>
        <View
          style={[
            style.bubble,
            {
              backgroundColor: 'lightblue',
              marginTop: 3,
              marginBottom: 'auto',
              marginLeft: 2,
            },
          ]}>
          <Text style={style.bubbleText}>AI</Text>
        </View>
      </View>
    </View>
  );

  return (
    <View style={style.container}>
      <View style={{flex: 9}}>
        <Button onPress={cleanChatHistory} title="Clear Chat" />
        <FlatList
          data={message}
          renderItem={renderItem}
          keyExtractor={(item, index) => item.question + index}
          // inverted
        />
      </View>

      <View
        style={{
          flexDirection: 'row',
          justifyContent: 'space-around',
          backgroundColor: '#9E99A9',
          paddingBottom: 30,
          paddingTop: 10,
          alignItems: 'center',
          flexGrow: 1,
          flex: 0.5,
        }}>
        <TextInput
          placeholder="Type a message"
          value={userInput}
          onChangeText={value => setUserInput(value)}
          onSubmitEditing={sendMessage}
          style={style.textInputStyle}
          placeholderTextColor="black"
        />
        <Button
          disabled={loading}
          onLongPress={cleanChatHistory}
          onPress={sendMessage}
          title="Send"
        />
      </View>
    </View>
  );
};
export default AboutScreen;
const style = StyleSheet.create({
  container: {
    flex: 1,
    flexGrow: 10,
    backgroundColor: 'white',
  },
  textInputStyle: {
    borderColor: 'black',
    borderWidth: 1,
    backgroundColor: 'gray',
    borderRadius: 5,
    padding: 10,
    flex: 0.8,
    backgroundColor: '#f0f0f0',
  },
  //Chat style starts
  chatContainer: {
    margin: 10,
  },
  bubbleMainContainer: {
    flexDirection: 'row',
  },
  bubble: {
    borderRadius: 100,
    backgroundColor: 'grey',
    width: 20,
    height: 20,
  },
  bubbleText: {
    color: 'white',
    textAlign: 'center',
    marginTop: 2,
  },
  messageHistory: {
    // flex: 1,
    // backgroundColor: "#9DEFD6",
    padding: 6,
    fontSize: 18,
    color: 'black',
    // margin: 10,
    textAlign: 'left',
    // borderRadius: 10,
  },
  userInputContainer: {
    flexDirection: 'row',
    borderRadius: 5,
    backgroundColor: '#B0F4EE',
    marginTop: 5,
  },
  userInputTextDesign: {
    padding: 4,
    textAlign: 'left',
    color: 'black',
    fontWeight: 'bold',
    fontSize: 16,
  },
});
